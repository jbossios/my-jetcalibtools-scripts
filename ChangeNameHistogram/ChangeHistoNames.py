
import sys
import re
import array
import os
from ROOT import *
def GetKeyNames(self,dir=""):
    self.cd(dir)
    return [key.GetName() for key in gDirectory.GetListOfKeys()]
TFile.GetKeyNames = GetKeyNames

if len(sys.argv) != 5:
    print "Incorrect number of arguments.  Expected the following:"
    print "     1. Input root file"
    print "     2. Output root file"
    print "     3. String to find (such as TopoEM)"
    print "     4. Replacement string (such as EMTopo)"
    exit(1)

inFileName  = sys.argv[1]
outFileName = sys.argv[2]
findStr     = sys.argv[3]
replaceStr  = sys.argv[4]

# Safety checks
if not inFileName.endswith(".root"):
    print "Input file doesn't appear to be a root file:",inFileName
    print "Blocking for safety"
    exit(2)
if not outFileName.endswith(".root"):
    print "Output file doesn't appear to be a root file:",outFileName
    print "Blocking for safety"
    exit(3)
if os.path.isfile(outFileName):
    print "Output file already exists:",outFileName
    print "Blocking for safety"
    exit(4)


inFile  = TFile.Open(inFileName,"READ")
outFile = TFile.Open(outFileName,"RECREATE")

# Check all histograms in the file
for histName in inFile.GetKeyNames():
    hist = inFile.Get(histName)
    # Check if the histogram name contains the string
    if findStr in histName:
        newHistName = re.sub(findStr,replaceStr,histName)
        hist.SetName(newHistName)
        
        # Also adjust the histogram title if applicable
        histTitle = hist.GetTitle()
        if findStr in histTitle:
            newHistTitle = re.sub(findStr,replaceStr,hist.GetTitle())
            hist.SetTitle(newHistTitle)
        
        # Write the name-changed histogram
        outFile.cd()
        hist.Write(newHistName)
    else:
        # Doesn't contain the string, just write it out
        outFile.cd()
        hist.Write(histName)

outFile.Close()
inFile.Close()

