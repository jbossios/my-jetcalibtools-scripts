import os
import sys
import ROOT
from typing import Union

def get_histogram(input_file_name: str, hist_name: str) -> Union[ROOT.TH1D, ROOT.TH2D]:
    # Open input file
    input_file = ROOT.TFile.Open(input_file_name)
    if not input_file:
        print(f"{input_file_name} couldn't be opened, exiting")
        sys.exit(1)
    # Get histogram
    hist = input_file.Get(hist_name)
    if not hist:
        print(f'ERROR: {hist_name} can not be found in {input_file_name}, exiting')
        sys.exit(1)
    hist.SetDirectory(0)
    input_file.Close()
    return hist


def get_dummy_absolute_insitu_hist(jetcoll: str) -> ROOT.TH1D:
    input_file_name = f'/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/SmallR/EmptyAbsoluteInsitu/{jetcoll}_DummyAbsoluteInsitu.root'
    hist_name = f'{jetcoll}_InsituCalib'
    return get_histogram(input_file_name, hist_name)


def get_etaintercal_hist(path: str, jetcoll: str) -> ROOT.TH2D:
    input_files = os.listdir(path)
    if len(input_files) > 1:
        print(f"ERROR: More than one file was found in {path}, don't know which one to pick up, exiting")
        sys.exit(1)
    input_file_name = f'{path}{input_files[0]}'
    hist_name = f'{jetcoll}_EtaInterCalibration'
    return get_histogram(input_file_name, hist_name)


def create_file(path: str, jetcoll: str, data_year: str, output_path: str):
    """ Unify eta-intercalibration ROOT file with dummy absolute insitu ROOT file """
    # Get eta-intercalibration histogram
    etaintercal_hist = get_etaintercal_hist(path, jetcoll)
    # Get dummy absolute insitu histogram
    absolute_insitu_hist = get_dummy_absolute_insitu_hist(jetcoll)
    # Create output file and save histograms
    out_file_name = f'{output_path}EtaIntercalOnly_{data_year}.root'
    print(f'INFO: writing {out_file_name}...')
    out_file = ROOT.TFile(out_file_name, 'RECREATE')
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    out_file.cd()
    etaintercal_hist.Write()
    absolute_insitu_hist.Write()
    out_file.Close()
  

if __name__ == '__main__':
    jet_collection = 'AntiKt4EMPFlow'  # should not end on 'Jets'
    input_path = '/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/SmallR/EtaIntercalibrationInputs/Louis_11052022_nominalOnly/'
    output_path = '/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/SmallR/EtaIntercalibrationOnlyROOTfiles/Louis_11052022/'
    data_years = ['data1516', 'data17', 'data18']  # these should be folders inside input_path
    for data_year in data_years:
        print(f'INFO: processing {data_year}...')
        path = '{}{}/'.format(input_path, data_year)
        create_file(path, jet_collection, data_year, output_path)
    print('>>> ALL DONE <<<')
