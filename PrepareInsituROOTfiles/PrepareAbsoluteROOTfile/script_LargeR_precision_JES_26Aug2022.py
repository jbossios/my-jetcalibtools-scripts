import ROOT
import array

def make_root_file(inpath, jetcoll, outputfilename):

    # Open Input File
    input_file_name = inpath + 'Correction.txt'
    print('INFO: Reading correction factors from {}'.format(input_file_name))
    low_edge_values = []
    upper_edge_values = []
    corr_values = []
    with open(input_file_name, 'r') as ifile:
        for line in ifile.readlines():
          values = line.split(' ')
	  low_edge_values.append(float(values[0]))
	  upper_edge_values.append(float(values[1]))
	  corr_values.append(float(values[2]))

    # Prepare output histogram
    bins = low_edge_values + [upper_edge_values[-1]]
    hist_name1 = '{}_InsituCalib'.format(jetcoll)
    hist_name2 = "In-situ calibration for {}".format(jetcoll)
    nbins = len(bins) - 1
    hist = ROOT.TH1D(hist_name1, hist_name2, nbins, array.array('d', bins))
    hist.Sumw2()
    for i, val in enumerate(corr_values, 1):
      hist.SetBinContent(i, val)

    # Write histogram into a ROOT file
    outfile = ROOT.TFile(outputfilename, 'RECREATE')
    print('INFO: Writing in situ histogram into {}'.format(outputfilename))
    outfile.cd();
    hist.Write()
    outfile.Close()


if __name__ == '__main__':

    input_path = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/LargeR/JEScombination/Mariana_25082022/"
    jetcoll = "AntiKt10UFOCSSKSoftDropBeta100Zcut10"
    outputfile = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/LargeR/JEScombination/Mariana_25082022/InsituCalibration_FullRun2data_Aug_2022_AntiKt10UFOCSSKSoftDropBeta100Zcut10_AbsoluteOnly.root"
    make_root_file(input_path, jetcoll, outputfile)
