import ROOT
import sys

def make_final_root_file(**kargs):
    """Unify eta-intercalibration and absolute in situ inputs"""

    data_period = kargs['data_period']
    jet_algo = kargs['jet_algo']
    absolute_input = kargs['absolute_input']
    relative_input = kargs['relative_input']
    output_name = kargs['output_name']

    print('INFO: Producing {}...'.format(output_name))
 
    # Get absolute insitu histogram
    abs_file = ROOT.TFile.Open(absolute_input)
    if not abs_file:
        print('ERROR: {} not found, exiting'.format(absolute_input))
        sys.exit(1)
    abs_hist_name = '{}_InsituCalib'.format(jet_algo)
    abs_hist = abs_file.Get(abs_hist_name)
    abs_hist.SetDirectory(0)
    abs_file.Close()

    # Get relative insitu histogram (i.e. eta-intercalibration)
    rel_file = ROOT.TFile.Open(relative_input)
    if not rel_file:
        print('ERROR: {} not found, exiting'.format(relative_input))
        sys.exit(1)
    rel_hist_name = '{}_EtaInterCalibration'.format(jet_algo)
    rel_hist = rel_file.Get(rel_hist_name)
    rel_hist.SetDirectory(0)
    rel_file.Close()

    # Create output ROOT file
    out_file = ROOT.TFile(output_name, 'RECREATE')
    out_file.cd()
    abs_hist.Write()
    rel_hist.Write()
    out_file.Close()


if __name__ == '__main__':
    # Settings
    data_periods = ['1516', '17', '18']  # supported options: '1516', '17' and '18'
    jet_algo = 'AntiKt10UFOCSSKSoftDropBeta100Zcut10'  # options: 'AntiKt4EMPFlow' and 'AntiKt10UFOCSSKSoftDropBeta100Zcut10'
    # Run
    for data_period in data_periods:
        absolute_input = '/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/LargeR/JEScombination/Mariana_25082022/InsituCalibration_FullRun2data_Aug_2022_AntiKt10UFOCSSKSoftDropBeta100Zcut10_AbsoluteOnly.root'
        relative_input = '/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/LargeR/EtaIntercalibrationInputs/Sasha_24082022/data{}/PowhegPythia8_PowhegHerwig7Angular_nominal_EtaIntercalibration13TeV25ns_Eta1516Opt_MM_AntiKt10UFOCSSKSoftDropBeta100Zcut10_over_AntiKt10UFOCSSKSoftDropBeta100Zcut10_Calibration.root'.format(data_period)  # i.e. eta-intercalibration input
        output_name = '/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu/CalibInputs/LargeR/FinalInsitu/InsituCalibration_AntiKt10UFOCSSKSoftDropBeta100Zcut10_Precision_Aug2022_{}.root'.format(data_period)
        make_final_root_file(
            data_period = data_period,
            jet_algo = jet_algo,
            absolute_input = absolute_input,
            relative_input = relative_input,
            output_name = output_name,
        )
