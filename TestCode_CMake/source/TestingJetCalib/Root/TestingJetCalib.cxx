/* **********************************************************************\
 *                                                                      *
 *      Name: TestingJetCalib                                           *
 *      Purpose: Test Calibrations					*
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 07/11/14  First Version              J. Bossio (jbossios@cern.ch) * 
\************************************************************************/

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TestingJetCalib/TestingJetCalib.h>

//xAOD EDM classes
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventShape/EventShape.h"
#include <AsgTools/MessageCheck.h> // Needed for ANA_CHECK

#include "JetCalibTools/JetCalibrationTool.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TestingJetCalib)


//--------------
// Constructor
//--------------
TestingJetCalib :: TestingJetCalib (){
  
  m_isCollisions 	  = false;
  m_recoJetCollectionName = "AntiKt4EMTopo";
  m_jetCalibFile          = "JES_Prerecommendation2015_Feb2015.config";
  m_calibSequence         = "JetArea_Residual_EtaJES_GSC";
  m_calibSequence         = "00-04-81";
  m_DEVmode		  = false;
  
}

TestingJetCalib :: TestingJetCalib (bool isCollisions, std::string recoJetCollectionName, std::string jetCalibFile, std::string calibSequence, std::string calibArea, bool DEVmode)
{

  //---------------------------
  //  Configuration variables
  //---------------------------

  m_isCollisions                = isCollisions;
  m_recoJetCollectionName       = recoJetCollectionName;
  m_jetCalibFile                = jetCalibFile;
  m_calibSequence               = calibSequence;
  m_calibArea                   = calibArea;
  m_DEVmode			= DEVmode;

}

EL::StatusCode TestingJetCalib :: setupJob (EL::Job& job){

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  
  job.useXAOD ();

  // let's initialize the algorithm to use the xAODRootAccess package
  ANA_CHECK(xAOD::Init());
   
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TestingJetCalib :: histInitialize ()
{
  
  //-------------------------------------------------------------
  // Initializing a Timer to keep track of processing rate
  //-------------------------------------------------------------

  m_Stopwatch = new TStopwatch();
  m_Stopwatch->Start();

  //---------------------------
  // Initiate Histograms
  //---------------------------

  h_reco_pT_distribution = new TH1D("h_reco_pT_distribution","",250,0,2500);
  wk()->addOutput (h_reco_pT_distribution);
  h_reco_eta_distribution = new TH1D("h_reco_eta_distribution","",100,-5,5);
  wk()->addOutput (h_reco_eta_distribution);
  h_reco_phi_distribution = new TH1D("h_reco_phi_distribution","",64,-3.2,3.2);
  wk()->addOutput (h_reco_phi_distribution);
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TestingJetCalib :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TestingJetCalib :: changeInput (bool /*firstFile*/)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TestingJetCalib :: initialize ()
{

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)

  // Count number of events
  m_eventCounter = 0;

  std::cout << "Configuration:" << std::endl;
  std::cout << "m_isCollisions = " << m_isCollisions << std::endl;
  std::cout << "m_recoJetCollectionName = " << m_recoJetCollectionName << std::endl;
  std::cout << "m_jetCalibFile = " << m_jetCalibFile << std::endl;
  std::cout << "m_calibSequence = " << m_calibSequence << std::endl;
  std::cout << "m_calibArea = " << m_calibArea << std::endl;
  std::cout << "m_DEVmode = " << m_DEVmode << std::endl;

  //--------------------------------------------------------
  // Initialization of JetCalibTools for EtaJES_GSC_Mass
  //--------------------------------------------------------

  const std::string name_to_JetCalibTools_JES = "Testing"; //string describing the current thread, for logging
  // Verbosity
  //m_JetCalibrationTool_JES->msg().setLevel( MSG::INFO); //other options: VERBOSE, DEBUG, ERROR, INFO, WARNING

  //asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;

  //JetCalibrationTool_handle->setTypeAndName("JetCalibrationTool/Testing");
  JetCalibrationTool_handle.setName("JetCalibrationTool/Testing");
  ANA_MSG_DEBUG("Trying to set-up tool: " << JetCalibrationTool_handle.typeAndName());
  ANA_CHECK( ASG_MAKE_ANA_TOOL(JetCalibrationTool_handle, JetCalibrationTool) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("JetCollection",m_recoJetCollectionName.Data()) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("ConfigFile",m_jetCalibFile.Data()) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibSequence",m_calibSequence.Data()) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibArea",m_calibArea.Data()) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("IsData",m_isCollisions) );
  if(m_DEVmode) ANA_CHECK( JetCalibrationTool_handle.setProperty("DEVmode",true) );
  ANA_CHECK( JetCalibrationTool_handle.setProperty("OutputLevel", MSG::INFO));
  ANA_CHECK( JetCalibrationTool_handle.retrieve() );

  m_event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TestingJetCalib :: execute ()
{

  m_eventCounter++;
  
  // Print every 1000 events, so we know where we are:
  if( (m_eventCounter % 1000) ==0 ) std::cout << "Event number = " << m_eventCounter << std::endl;

  // Deleting and Clearing all objects for the event
  for (unsigned int i =0; i< m_recoJets.size();  i++) delete m_recoJets.at(i); m_recoJets.clear();

  //--------------
  // Getting Jets
  //--------------

  const xAOD::JetContainer* reco_jets = 0;
  if(m_recoJetCollectionName == "AntiKt4LCTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4LCTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt4EMTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else if(m_recoJetCollectionName.Contains("CamKt")){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt4EMTopoTrig"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt10LCTopoTrimmedPtFrac5SmallR30"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt10LCTopoTrimmedPtFrac5SmallR30Jets"));
  }
  else if(m_recoJetCollectionName == "AntiKt10LCTopoTrimmedPtFrac5SmallR20"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"));
  }
  else if(m_recoJetCollectionName == "AntiKt4EMPFlow"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMPFlowJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt2LCTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt2LCTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt3LCTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt3LCTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt5LCTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt5LCTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt6LCTopo"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt6LCTopoJets"));
  } 
  else if(m_recoJetCollectionName == "AntiKt4HI"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt2HI"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else if(m_recoJetCollectionName == "AntiKt3HI"){
    ANA_CHECK(m_event->retrieve( reco_jets, "AntiKt4EMTopoJets"));
  }
  else {
    Error("execute()", "Wrong container. Exiting." );
  }

  // Loop over the jets in the container
  for ( auto *ijet : *reco_jets ) {

   	//std::cout << "Mass before Correction = " << (*jet_itr)->m() << std::endl;

	// Calibrating Jet
        IJet* J = new IJet();
        xAOD::Jet * jet = 0;
        //m_JetCalibrationTool_JES->calibratedCopy(**jet_itr,jet); //make a calibrated copy
        //m_JetCalibrationTool_JES->calibratedCopy(*ijet,jet); //make a calibrated copy
	JetCalibrationTool_handle->calibratedCopy(*ijet,jet); //make a calibrated copy

	/* 
	std::cout << "pT: " << ijet->pt()*0.001 << std::endl;
	xAOD::JetFourMom_t jetPileupP4 = jet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum");
	std::cout << "pT_after_Pileup: " << jetPileupP4.Pt()*0.001 << std::endl;
	std::cout << "pT_calib: " << jet->pt()*0.001 << std::endl;
	std::cout << "Eta: " << ijet->eta() << std::endl;
	std::cout << "Eta_after_Pileup: " << jetPileupP4.Eta() << std::endl;
	std::cout << "Eta_calib: " << jet->eta() << std::endl;
	std::cout << "Phi: " << ijet->phi() << std::endl;
	std::cout << "Phi_calib: " << jet->phi() << std::endl;
	std::cout << "E: " << ijet->e()*0.001 << std::endl;
	std::cout << "E_calib: " << jet->e()*0.001 << std::endl;
	*/
	
        J->SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());

	// Filling distribution histograms
  	h_reco_pT_distribution->Fill( J->Pt()*0.001 );
  	h_reco_eta_distribution->Fill( J->Eta() );
  	h_reco_phi_distribution->Fill( J->Phi() );
        m_recoJets.push_back(J);

	xAOD::JetFourMom_t jet_Constit_P4 = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
	xAOD::JetFourMom_t jet_Origin_P4;
	xAOD::JetFourMom_t jet_Pileup_P4;
	xAOD::JetFourMom_t jet_EtaJES_P4;
	xAOD::JetFourMom_t jet_GSC_P4;
	xAOD::JetFourMom_t jet_Insitu_P4;
	xAOD::JetFourMom_t jet_JMS_P4;
	xAOD::JetFourMom_t jet_JMS_correlated_P4;

	bool Origin  = false;
	bool JetArea = false;
        bool EtaJES  = false;
	bool GSC     = false; 
	bool Insitu  = false;
	bool JMS     = false;

	if(m_calibSequence.Contains("Origin"))  Origin  = true;  
	if(m_calibSequence.Contains("JetArea")) JetArea = true;
	if(m_calibSequence.Contains("EtaJES"))  EtaJES  = true;
	if(m_calibSequence.Contains("GSC"))     GSC     = true;
	if(m_calibSequence.Contains("Insitu"))  Insitu  = true;
	if(m_calibSequence.Contains("JMS"))     JMS     = true;

        
	if(Origin)  jet_Origin_P4 = jet->getAttribute<xAOD::JetFourMom_t>("JetOriginConstitScaleMomentum");
	if(JetArea) jet_Pileup_P4 = jet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum");
	if(EtaJES)  jet_EtaJES_P4 = jet->getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum");
	if(GSC)     jet_GSC_P4    = jet->getAttribute<xAOD::JetFourMom_t>("JetGSCScaleMomentum");
	if(Insitu)  jet_Insitu_P4 = jet->getAttribute<xAOD::JetFourMom_t>("JetInsituScaleMomentum");
	if(JMS)     jet_JMS_P4    = jet->getAttribute<xAOD::JetFourMom_t>("JetJMSScaleMomentum");
	//if(JMS)     jet_JMS_P4    = jet->getAttribute<xAOD::JetFourMom_t>("JetJMSScaleMomentumCombQCD");
	//if(JMS)     jet_JMS_P4    = jet->getAttribute<xAOD::JetFourMom_t>("JetJMSScaleMomentumCombQCDCorrelatedWeights");

	
	// Consit Scale
	std::cout << "pT  constit Scale = " << jet_Constit_P4.Pt()*0.001 << std::endl;
	std::cout << "E   constit Scale = " << jet_Constit_P4.E()*0.001 << std::endl;
	std::cout << "Eta constit Scale = " << jet_Constit_P4.Eta() << std::endl;
	if(JMS) std::cout << "Mass constit Scale = " << jet_Constit_P4.M()*0.001 << std::endl;

	// Origin Scale
	if(Origin){
	  std::cout << "pT  after Origin = " << jet_Origin_P4.Pt()*0.001 << std::endl;
	  std::cout << "E   after Origin = " << jet_Origin_P4.E()*0.001 << std::endl;
	  std::cout << "Eta after Origin = " << jet_Origin_P4.Eta() << std::endl;
	  if(JMS) std::cout << "Mass after Origin = " << jet_Origin_P4.M()*0.001 << std::endl;
	}
	// Pileup Scale
	if(JetArea){
	  std::cout << "pT  after Pileup = " << jet_Pileup_P4.Pt()*0.001 << std::endl;
	  std::cout << "E   after Pileup = " << jet_Pileup_P4.E()*0.001 << std::endl;
	  std::cout << "Eta after Pileup = " << jet_Pileup_P4.Eta() << std::endl;
	  if(JMS) std::cout << "Mass after Pileup = " << jet_Pileup_P4.M()*0.001 << std::endl;
	}
	// JES Scale
	if(EtaJES){
	  std::cout << "pT  after JES = " << jet_EtaJES_P4.Pt()*0.001 << std::endl;
	  std::cout << "E   after JES = " << jet_EtaJES_P4.E()*0.001 << std::endl;
	  std::cout << "Eta after JES = " << jet_EtaJES_P4.Eta() << std::endl;
	  if(fabs(jet_EtaJES_P4.Eta() - jet_Constit_P4.Eta())>1) std::cout << "ERROR: Big Eta Corr!" << std::endl;
	  if(JMS) std::cout << "Mass after JES = " << jet_EtaJES_P4.M()*0.001 << std::endl;
	}
	// GSC Scale
	if(GSC){
	  std::cout << "pT  after GSC = " << jet_GSC_P4.Pt()*0.001 << std::endl;
	  std::cout << "E   after GSC = " << jet_GSC_P4.E()*0.001 << std::endl;
	  std::cout << "Eta after GSC = " << jet_GSC_P4.Eta() << std::endl;
	  if(JMS) std::cout << "Mass after GSC = " << jet_GSC_P4.M()*0.001 << std::endl;
	}
        // Insitu Scale
	if(Insitu){
	  std::cout << "pT  after Insitu = " << jet_Insitu_P4.Pt()*0.001 << std::endl;
	  std::cout << "E   after Insitu = " << jet_Insitu_P4.E()*0.001 << std::endl;
	  std::cout << "Eta after Insitu = " << jet_Insitu_P4.Eta() << std::endl;
	  std::cout << "Mass after Insitu = " << jet_Insitu_P4.M()*0.001 << std::endl;
	}
	// JMS Scale
	if(JMS){	
	  std::cout << "pT  after JMS = "  << jet_JMS_P4.Pt()*0.001 << std::endl;
	  std::cout << "E after JMS = " << jet_JMS_P4.E()*0.001 << std::endl;
	  std::cout << "Mass after JMS = " << jet_JMS_P4.M()*0.001 << std::endl;
	  //std::cout << "pT  after JMS (correlatedWeights) = "  << jet_JMS_correlated_P4.Pt()*0.001 << std::endl;
	  //std::cout << "Mass after JMS (correlatedWeights) = " << jet_JMS_correlated_P4.M()*0.001 << std::endl;
	}
	std::cout << "#######################################" << std::endl;
        
	// JMS Scale
	/*
	std::cout << "Uncalibrated mTA: " << jet->getAttribute<float>("JetTrackAssistedMassUnCalibrated") << std::endl;
	std::cout << "Calibrated mTA: " << jet->getAttribute<float>("JetTrackAssistedMassCalibrated") << std::endl;
	std::cout << "pT Corr by mTA: " << jet->getAttribute<float>("JetpTCorrByCalibratedTAMass") << std::endl;
	std::cout << "#######################################" << std::endl;
	*/

        delete jet;
	
  } //END: Loop over jets

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TestingJetCalib :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TestingJetCalib :: finalize ()
{
 
       /*	
  if( m_JetCalibrationTool_JES ) {
         delete m_JetCalibrationTool_JES;
         m_JetCalibrationTool_JES = 0;
  }*/

  //-------------------
  // Stop the timer
  //-------------------

  m_Stopwatch->Stop();

  std::cout << "#################################################" << std::endl;

  std::cout << "Number of events         	= " << m_eventCounter << std::endl;

  std::cout << "Event processing rate (Hz)      = " << m_eventCounter / m_Stopwatch->RealTime() << std::endl;

  std::cout << "Time for everthing              = " << m_Stopwatch->RealTime() << std::endl;
  
  std::cout << "Done!" << std::endl;

  std::cout << "#################################################" << std::endl;
   
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TestingJetCalib :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


