////////////////////////////////////////////////////////////////////////
// ExtdTLV: Extended TLorentzVector
////////////////////////////////////////////////////////////////////////
// (C) 2012, Atlas/Clermont team
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "../TestingJetCalib/ExtdTLV.h"

using namespace std;

ExtdTLV::ExtdTLV() :
  TLorentzVector(),
  m_index(-1),
  m_moments()
{}

ExtdTLV::ExtdTLV(const ExtdTLV &etlv) :
  TLorentzVector(etlv),
  m_index(etlv.m_index),
  m_moments(etlv.m_moments)
{}

ExtdTLV::ExtdTLV(const TLorentzVector &tlv) :
  TLorentzVector(tlv),
  m_index(-1),
  m_moments()
{}

ExtdTLV::ExtdTLV(const double px,const double py,const double pz,const double e) : 
  TLorentzVector(px,py,pz,e),
  m_index(-1),
  m_moments()
{}

ExtdTLV::~ExtdTLV()
{}

ExtdTLV &ExtdTLV::operator=(const ExtdTLV &etlv)
{
  if (this!=&etlv) {
    TLorentzVector::operator=(etlv);
    m_index=etlv.m_index;
    m_moments=etlv.m_moments;
  }
  return *this;
}

double ExtdTLV::getMoment(const string &name) const
{
  map<string,double>::const_iterator it=m_moments.find(name);
  if (it!=m_moments.end()) return it->second;

  cout << " >>>>> FATAL in ExtdTLV::getMoment(''" <<  name << "''): unknown moment !" << endl;
  return -1; 
}

double ExtdTLV::rapidity() const
{
  if ((E()-Pz())<=0) return -1e9;
  return Rapidity();
}

void ExtdTLV::add4Mom(const double px,const double py,const double pz,const double e)
{
  SetPx(Px()+px);
  SetPy(Py()+py);
  SetPz(Pz()+pz);
  SetE(E()+e);
}

void ExtdTLV::rescaleEnergy(const double alpha)
{
  const double e=E()*alpha;
  const double eta=Eta();
  const double phi=Phi();
  const double m=M();
  const double pt=TMath::Sqrt(e*e-m*m)/TMath::CosH(eta);
  SetPtEtaPhiE(pt,eta,phi,e);
}

void ExtdTLV::rescaleMomentum(const double alpha)
{
  const double px=Px()*alpha;
  const double py=Py()*alpha;
  const double pz=Pz()*alpha;
  const double m=M();
  const double e=TMath::Sqrt(m*m+px*px+py*py+pz*pz);
  SetPxPyPzE(px,py,pz,e);
}

void ExtdTLV::print() const
{
  cout << "(px,py,pz,e)=(" << Px() << "," << Py() << "," << Pz()
       << "," << E() << ")" << endl;
}

TLorentzVector operator+(const ExtdTLV &etlv,const TLorentzVector &tlv)
{
  TLorentzVector result=etlv;
  result+=tlv;
  return result;
}

TLorentzVector operator-(const ExtdTLV &etlv,const TLorentzVector &tlv)
{
  TLorentzVector result=etlv;
  result-=tlv;
  return result;
}


