/* **********************************************************************
 *                                                                      *
 *     Name           : IJet
 *                                                                      *
 *     Task           :

 *     Arguments      : 
 *                                                                      *
 *     Edition History                                                  *
 *                                                                      *
 *  #   Date    Comments                                       By       *
 * -- -------- ---------------------------------------------- --------- *
 *  0 10/10/00                                                N.Ghodbane

 ************************************************************************/
#include <iostream>

#include "../TestingJetCalib/IJet.h"

IJet::IJet(): ExtdTLV(){
  m_muInside_index.clear();
  m_collType=0; //default unknown
}

IJet::IJet(const IJet &jet) :
  ExtdTLV(jet),
  m_muInside_index(jet.m_muInside_index),
  m_constituent_index(),  // WHY NOT COPYING ?
  m_collType(0)  // WHY NOT COPYING ?
{}

IJet::IJet(double x, double y, double z, double e): ExtdTLV(x,y,z,e){
  m_constituent_index.clear();
}

IJet::~IJet(){
}
void IJet::addConstituent(int index){
  m_constituent_index.push_back(index);
  return;
}

//_________________________________________________________________________________________________
IJet &IJet::operator=(const IJet & q) {
  this->SetPx(q.Px());
  this->SetPy(q.Py());
  this->SetPz(q.Pz());
  this->SetE (q.E() );
  return *this;
}

void IJet::print(){
  std::cout << "(pt,eta,phi,e,SV0,Q,n90) = " << "0x" << this << "\t" << this->getIndex() << "\t (" << this->pt() << "," << this->eta() << "," << this->phi() << "," <<  this->e() << "," 
	    << "\t" << this->getMoment("SV0") 
	    << "\t" << "," << this->getMoment("isLooseBad") 
	    //<< "\t" << "," << this->getMoment("n90") 
	    //<< "\t" << "," << this->getMoment("EMF")
	    //<< "\t" << "," << this->getMoment("hecf")			
	    //<< "\t" << "," << this->getMoment("tileGap3F")		
	    << "\t" << "," << this->getMoment("Timing")			
	    //<< "\t" << "," << this->getMoment("quality")		
	    //<< "\t" << "," << this->getMoment("LArQuality")		
	    //<< "\t" << "," << this->getMoment("HECQuality")		
	    //<< "\t" << "," << this->getMoment("fracSamplingMax")	
	    << ")" << std::endl;
  return;
}

IJet* IJet::getInstance(){
  return new IJet(*this);
}

std::string IJet::getCollectionName(){
  std::string collName="Unknown";
  if (m_collType==10400) collName="AntiKt4TopoEMJets";
  if (m_collType==10401) collName="AntiKt4TopoJets";
  if (m_collType==10402) collName="AntiKt4LCTopoJets";
  if (m_collType==10403) collName="AntiKt4TowerJets";
  if (m_collType==10404) collName="AntiKt4LCTopoCBJets";

  if (m_collType==10600) collName="AntiKt6TopoEMJets";
  if (m_collType==10601) collName="AntiKt6TopoJets";
  if (m_collType==10602) collName="AntiKt6LCTopoJets";
  if (m_collType==10603) collName="AntiKt6TowerJets";
  if (m_collType==10404) collName="AntiKt6LCTopoCBJets";

  if (m_collType==20400) collName="AntiKt4TrackZJets"; 
  if (m_collType==20600) collName="AntiKt6TrackZJets";

  if (m_collType==30400) collName="AntiKt4TruthJets";
  if (m_collType==30600) collName="AntiKt6TruthJets";


 return collName;
}

