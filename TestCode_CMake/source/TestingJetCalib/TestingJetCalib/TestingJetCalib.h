#ifndef TestingJetCalib_TestingJetCalib_H
#define TestingJetCalib_TestingJetCalib_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"

//JetCalibrationTool
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

//_________________________________________________________________________________________________
// ROOT specific header files
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <TStopwatch.h>
#include <TRandom.h>
#include <TVector2.h>
#include <TLeaf.h>
#include <TString.h>
#include <TMath.h>
#include <TROOT.h>
#include <TList.h>

//_________________________________________________________________________________________________
// Personal
#include "IJet.h"

class TestingJetCalib : public EL::Algorithm{
public:

  xAOD::TEvent *m_event;  //!
  int m_eventCounter; //!
  TStopwatch* m_Stopwatch; //!

  /////////////
  // Histograms
  /////////////

  // reco jets
  TH1D* h_reco_pT_distribution;//!
  TH1D* h_reco_eta_distribution;//!
  TH1D* h_reco_phi_distribution;//!

  ///////////////////
  // Vectors for jets
  ///////////////////
  std::vector<IJet*> m_recoJets;//!

  // For JetCalibTools
  //JetCalibrationTool* m_JetCalibrationTool_JES; //!
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;//!
  TString m_recoJetCollectionName;
  TString m_jetCalibFile;
  TString m_calibSequence;
  TString m_calibArea;
  bool m_isCollisions;
  bool m_DEVmode;

  // Variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // Standard constructor
  TestingJetCalib ();
  TestingJetCalib (bool m_isCollisions_aux, std::string m_recoJetCollectionName_aux, std::string m_jetCalibFile_aux, std::string m_calibSequence_aux, std::string m_calibArea, bool m_DEVmode_aux);

  // Functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // This is needed to distribute the algorithm to the workers
  ClassDef(TestingJetCalib, 1);
};

#endif
