#ifndef IJet_H
#define IJet_H 1

#include <string>
#include <vector>

#include "ExtdTLV.h"

class IJet: public ExtdTLV {
 public:
  IJet();
  IJet(const IJet &jet);
  IJet(double, double, double, double);
  virtual ~IJet();
  void addConstituent(int);
  inline std::vector<int> getConstituentIndices() const { return m_constituent_index;};
  std::vector<int> m_muInside_index;
  std::vector<int> m_constituent_index;
  inline void scale(const double alpha) {TLorentzVector::operator*=(alpha);}
  virtual IJet* getInstance();
  inline int getCollectionType(){ return m_collType;}
  inline void setCollectionType(int collType){ m_collType=collType;}
  std::string getCollectionName();
  int m_collType;
  //_________________________________________________________________________________________________

  IJet& operator=(const IJet& );

  void print();
};
#endif

