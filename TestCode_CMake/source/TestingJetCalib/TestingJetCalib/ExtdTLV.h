////////////////////////////////////////////////////////////////////////
// ExtdTLV: Extended TLorentzVector
//  adding index and moments
////////////////////////////////////////////////////////////////////////
// (C) 2012, Atlas/Clermont team
////////////////////////////////////////////////////////////////////////

#ifndef EXTDTLV_H
#define EXTDTLV_H

#include <string>
#include <map>

#include <TLorentzVector.h>

class ExtdTLV : public TLorentzVector {

public:

  // constructors
  ExtdTLV();
  ExtdTLV(const ExtdTLV &etlv);
  ExtdTLV(const TLorentzVector &tlv);
  ExtdTLV(const double px,const double py,const double pz,const double e);

  // destructor
  virtual ~ExtdTLV();

  // operators
  ExtdTLV &operator=(const ExtdTLV &etlv);
  
  // moments management
  inline void setMoment(const std::string &name,const double value) {m_moments[name]=value;}
  double getMoment(const std::string &name) const;

  // index management
  inline void setIndex(const int idx) {m_index=idx;}
  inline int getIndex() const {return m_index;}

  // protected rapidity (against division by zero)
  double rapidity() const;

  // addition of 4-momentum
  void add4Mom(const double px,const double py,const double pz,const double e);

  // rescaling
  void rescaleEnergy(const double alpha);
  void rescaleMomentum(const double alpha);

  // printing content
  void print() const;

  // completely useless methods, left for backward compatibility
  // Comment by NG. these are indeed needed in case of template methods which need methods like pt(), eta(), phi(), etc...
  // and which are used with CLHEP::HepLorentzVector, TruthParticle and the objects which derive from this ExtdTLV class.
  // basically TLorentzVectors use Pt() instead of pt().
  inline double px() const {return Px();}
  inline double py() const {return Py();}
  inline double pz() const {return Pz();}
  inline double e() const {return E();}
  inline double p() const {return P();}
  inline double pt() const {return Pt();}
  inline double m() const {return M();}
  inline double eta() const {return Eta();}
  inline double phi() const {return Phi();}

private:
  int m_index;
  std::map<std::string,double> m_moments;
};

TLorentzVector operator+(const ExtdTLV &etlv,const TLorentzVector &tlv);
TLorentzVector operator-(const ExtdTLV &etlv,const TLorentzVector &tlv);

inline bool operator==(const ExtdTLV &etlv1,const ExtdTLV &etlv2)
{
  return (etlv1.Px()==etlv2.Px() && etlv1.Py()==etlv2.Py() && etlv1.Pz()==etlv2.Pz());
}

inline bool operator!=(const ExtdTLV &etlv1,const ExtdTLV &etlv2)
{
  return (etlv1.Px()!=etlv2.Px() || etlv1.Py()!=etlv2.Py() || etlv1.Pz()!=etlv2.Pz());
}

#endif // EXTDTLV_H

