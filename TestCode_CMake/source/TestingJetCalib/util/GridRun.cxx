#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <sstream>

#ifdef XAOD_STANDALONE
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#else
#include "POOLRootAccess/TEvent.h"
#include "StoreGate/StoreGateSvc.h"
#endif

#include "TestingJetCalib/TestingJetCalib.h"

void usage() {
  std::cout << "Running options:" << std::endl;
  std::cout << "        --help : To get the help you're reading" << std::endl;
  std::cout << "        --jetCalibFile= : Specify the JetCalibTools config" << std::endl;
  std::cout << "        --jetColl= : Specify the jet collection" << std::endl;
  std::cout << "        --calibSequence= : Specify the calibration sequence for JetCalibTools" << std::endl;
  std::cout << "        --calibArea= : Specify the calibration area tag for JetCalibTools, for instance 00-04-79" << std::endl;
  std::cout << "        --isCollisions=TRUE : Specify isData true for JetCalibTools" << std::endl;
  std::cout << "        --isCollisions=FALSE : Specify isData false for JetCalibTools" << std::endl;
  std::cout << "        --sample= : Specify input xAOD" << std::endl;
  std::cout << "        --submitDir= : Specify output folder" << std::endl;
  std::cout << "        Example: Run --jetCalibConfig=JES_2015dataset_recommendation_Feb2016.config --jetColl=AntiKt4EMTopo --calibSequence=JetArea_Residual_Origin_EtaJES_GSC --isCollisions=FALSE --submitDir=Output --sample=xAOD.root" << std::endl;
}

int main( int argc, char* argv[] ) {

   // Take the submit directory from the input if provided:
   std::string submitDir = "submitDir"; // Output file name
   std::string directory; // Sample directory
   std::string m_recoJetCollectionName;
   std::string m_jetCalibFile;
   std::string m_calibSequence;
   std::string m_calibArea;
   std::string m_output = "";
   bool m_isCollisions=false;
   bool m_DEVmode = "";

   //-------------------------
   // Decode the user settings
   //-------------------------
   for (int i=1; i< argc; i++){

     std::string opt(argv[i]); std::vector< std::string > v;

     std::istringstream iss(opt);

     std::string item;
     char delim = '=';

     while (std::getline(iss, item, delim)){
       v.push_back(item);
     }

     if ( opt.find("--help") != std::string::npos ) {
       usage(); return 0;
     }

     if ( opt.find("--jetCalibFile=") != std::string::npos) m_jetCalibFile=v[1];

     if ( opt.find("--jetColl=")   != std::string::npos) m_recoJetCollectionName = v[1];
     
     if ( opt.find("--calibSequence=")   != std::string::npos) m_calibSequence = v[1];
     
     if ( opt.find("--calibArea=")   != std::string::npos) m_calibArea = v[1];

     if ( opt.find("--DEVmode=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_DEVmode= true;
       if (v[1].find("FALSE") != std::string::npos) m_DEVmode= false;
     }
     
     if ( opt.find("--isCollisions=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_isCollisions= true;
       if (v[1].find("FALSE") != std::string::npos) m_isCollisions= false;
     }

     // Take the submit directory from the input:
     if ( opt.find("--submitDir=")   != std::string::npos) submitDir = v[1];
     
     if ( opt.find("--output=")   != std::string::npos) m_output = v[1];

     if ( opt.find("--sample=")   != std::string::npos) directory = v[1];

   }//End: Loop over input options

   if(directory==""){
     std::cout << "ERROR: Input Sample not specified, exiting" << std::endl;
     return 1;	   
   }
   if(m_recoJetCollectionName==""){
     std::cout << "ERROR: Jet Collection not specified, exiting" << std::endl;
     return 1;	   
   }
   if(m_jetCalibFile==""){
     std::cout << "ERROR: Jet Calibration Config not specified, exiting" << std::endl;
     return 1;	   
   }
   if(m_calibSequence==""){
     std::cout << "ERROR: Calibration Sequence not specified, exiting" << std::endl;
     return 1;	   
   }

   // Set up the job for xAOD access:
   xAOD::Init().ignore();

   // Construct the samples to run on:
   SH::SampleHandler sh;

   SH::scanRucio(sh,directory);

   // Set the name of the input TTree. It's always "CollectionTree"
   // for xAOD files.
   sh.setMetaString( "nc_tree", "CollectionTree" );

   // Print what we found:
   sh.print();

   // Create an EventLoop job:
   EL::Job job;
   job.sampleHandler( sh );

   // To automatically delete submitDir
   job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

   // Add our analysis to the job:
   TestingJetCalib* alg = new TestingJetCalib(m_isCollisions, m_recoJetCollectionName, m_jetCalibFile, m_calibSequence, m_calibArea, m_DEVmode);
   job.algsAdd( alg );

   // Run the job using the Prun driver:
   EL::PrunDriver driver;

   driver.options()->setString("nc_outputSampleName", m_output);

   // Submit the job using prun driver:
   driver.submitOnly( job, submitDir );

   return 0;
}
