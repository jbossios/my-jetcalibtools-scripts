cd source
setupATLAS
lsetup git
lsetup rucio "asetup AnalysisBase,21.2.0" panda
localSetupFAX
voms-proxy-init -voms atlas
cd ../build
cmake ../source
make -j
source x86_64-slc6-gcc62-opt/setup.sh
export ROOT_INCLUDE_PATH=/cvmfs/atlas.cern.ch/repo/sw/ASG/21.2/AnalysisBase/21.2.0/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include:$ROOT_INCLUDE_PATH
cd ..
