#!/usr/bin/python

import os, sys

param=sys.argv

##############
### SAMPLE ###
##############
sample = "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM1.e3668_s3126_r9364_r9315_p3141"

date = "21July2017_TestingDEV_3"

##################
### MC or Data ###
##################
#isCollisions = False
isCollisions = True

###########
### DEV ###
###########
#DEV = False
DEV = True

###################
### Config File ###
###################

JetCalibFiles = [
  #"Test_PFlow.config",
  #"JES_2015dataset_recommendation_Feb2016_Trigger.config",
  #"JES_Prerecommendation2015_Feb2015.config",
  #"JES_Prerecommendation2015_Feb2015_Internal.config",
  #"JES_MC15Prerecommendation_April2015.config",
  #"JES_2015dataset_recommendation_Feb2016.config",
  #"CamKt_JES_HTT.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6987_LC_Feb2016.config",
  #"JES_MC15Prerecommendation_Feb2016_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_Jan2016_trigger.config",
  #"JES_MC15Prerecommendation_Feb2016_EtaIntercalOnly_PFlow.config",
  #"JES_MC15Prerecommendation_April2015_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_April2015_withoutEtaIntercal.config",
  #"JES_MC15Prerecommendation_Rscan_Aug2015.config",
  #"JES_MC15Prerecommendation_December2015_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_December2015_EtaIntercalOnly_PFlow.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6987_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6992_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6993_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6994_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6995_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6996_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6997_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6998_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6999_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r7000_Nov2015.config",
  #"JES_MC15Prerecommendation_PFlow_July2015.config",
  #"JES_MC15Prerecommendation_AFII_June2015.config",
  #"JES_MC15recommendation_FatJet_June2015.config",
  #"JES_MC15recommendation_FatJet_track_assisted_January2016.config",
  #"JES_MC15Prerecommendation_April2015_without_GSC.config",
  #"JES_Prerecommendation2015_AFII_Apr2015.config",
  #"JES_Full2012dataset_May2014.config",
  #"JES_Prerecommendation2015_FatJet_Apr2015.config",
  #"FatJet_JES_2012_0.config",
  #"JES_Full2012dataset_InsituDerivation.config",
  #"JES_MC15Prerecommendation_April2015_GSCEM3.config",
  #"JES_MC15Prerecommendation_April2015_GSCEM3.config",
  #"HLLHC/JES_MC15_HLLHC_r7699_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7700_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7701_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7702_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7703_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7704_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7709_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7768_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7769_May2016_v2.config",
  #"JES_MC15cRecommendation_May2016.config", # 20.7 recommendations 4EM and 4LC
  #"JES_MC15cRecommendation_PFlow_Aug2016.config",
  #"JES_2016data_Oct2016_EtaIntercalOnly.config",
  #"JES_MC15cRecommendation_May2016_JMS.config", # 20.7 recommendations 4EM and 4LC with JMS (only for 4EM)
  #"JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
  #"JES_MC15_FatJet_Nov2016_QCDCombinationCorrelatedWeightsOptional.config",
  #"JES_MC15cRecommendation_May2016_rel21.config",
  #"JES_MC15c_HI_Nov2016.config",
  #"JES_data2016_data2015_Recommendation_Dec2016.config", # Final config for 2015+2016 data
  #"JES_data2016_data2015_Recommendation_Dec2016_rel21.config", # Pre recs config for 2015+2016 data for rel 21
  #"JES_data2016_data2015_Recommendation_Dec2016_JMS.config", # Final config for 2015+2016 data with JMS (only for 4LC jets, no JMS for LC jets yet)
  #"JES_MC15cRecommendation_May2016.config",
  #"JES_MC15cRecommendation_PFlow_EtaIntercalOnly_March2017.config", # EtaIntercal Only config for pflow and 2016 data
  "JES_MC16Recommendation_June2017.config",
]
######################
### Jet Collection ###
######################
JetCollections = [
  "AntiKt4EMTopo",
  #"AntiKt4EMTopoTrig",
  #"AntiKt4EMPFlow",
  #"AntiKt4LCTopo",
  #"AntiKt10LCTopoTrimmedPtFrac5SmallR30",
  #"AntiKt10LCTopoTrimmedPtFrac5SmallR20", # Standard FatJets
  #"AntiKt2LCTopo",
  #"AntiKt5LCTopo",
  #"AntiKt3LCTopo",
  #"AntiKt6LCTopo",
  #"CamKt020LCTopo",
  #"CamKt025LCTopo",
  #"CamKt030LCTopo",
  #"CamKt035LCTopo",
  #"CamKt040LCTopo",
  #"CamKt045LCTopo",
  #"CamKt050LCTopo",
  #"CamKt055LCTopo",
  #"CamKt060LCTopo",
  #"CamKt150LCTopo",
  #"AntiKt4HI",
  #"AntiKt2HI",
  #"AntiKt3HI",
]
######################
### Calib Sequence ###
######################
#CalibrationSequence = "JetArea_Residual"
CalibrationSequence = "JetArea_Residual_EtaJES" #(PFlow, rel21 4EM)
#CalibrationSequence = "JetArea_Residual_EtaJES_GSC" #(PFlow)
#CalibrationSequence = "JetArea_Residual_EtaJES_GSC_Insitu" #(PFlow)
#CalibrationSequence = "JetArea_Residual_Origin"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_Insitu"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_GSC"
#CalibrationSequence = "JetArea_Residual_EtaJES_GSC_Insitu"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_GSC_JMS_Insitu"
#CalibrationSequence = "JetArea_EtaJES_GSC"
#CalibrationSequence = "JetArea_EtaJES_GSC_Insitu" # Trigger
#CalibrationSequence = "EtaMassJES"
#CalibrationSequence = "EtaJES"
#CalibrationSequence = "EtaJES_JMS" # large-R jets
#CalibrationSequence = "EtaJES_Insitu" # HI jets

##################################################################################
#
##################################################################################

Output = "Outputs/Output_"
command = ""

for jetcoll in JetCollections:

  for config in JetCalibFiles:

    output = Output
    output += jetcoll
    output += "_"
    output += config
    
    command += "nohup GridRun "

    # For JetCalibTools
    command += "--jetCalibFile=" + config

    command += " --jetColl=" + jetcoll
  
    command += " --calibSequence=" + CalibrationSequence

    if DEV:
      command += " --DEVmode=TRUE"

    if isCollisions:
      command += " --isCollisions=TRUE"

    # Outputs
    command += " --output=user.jbossios."
    command += date

    # Outputs (local)
    command += " --submitDir="
    command += output

    # Sample
    command += " --sample=" + sample + " > "

    # Logfile's Path
    command += "LogFiles/log_"
    command += jetcoll
    command += "_"
    command += config
    command += " 2> "

    # Errors File's Path
    command += "ErrFiles/errors_"
    command += jetcoll
    command += "_"
    command += config
    command += " && "

command = command[:-2]
print command
os.system(command)

