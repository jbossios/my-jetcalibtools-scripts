#!/usr/bin/python

import os, sys

param=sys.argv

##############
### SAMPLE ###
##############
# pcuba002
# 20.7 2015 data
#sample = "/4/jbossios/JetCalibTools_TestSamples/20_7_2015"
# 2016 data
#sample = "/4/jbossios/JetCalibTools_TestSamples/2016"
# MC15c 
#sample = "/4/jbossios/JetCalibTools_TestSamples/MC15c_Pythia"

# Lxplus
#sample = "/afs/cern.ch/user/j/jbossios/work/public/DAOD_JETM1_testfile_11_02_15_more_statistics"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/CalibAreaTags/JetCalibTools/DC14_DxAOD_test_file"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/MC15_AOD_JZ3_16_04_2015"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/Files_for_Testing/Muscan_SAMPLE"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/Files_for_Testing/JMS_sample"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/Files_for_Testing/JETM8_TrackAssistedMass_4EM" # FatJets"

################
# pcuba001
# For Rscan
#sample = "/1/jbossios/RScan_Sample"
# Dunno
#sample = "/2/test_file"
# LC Jets
#sample = "/3/jbossios/LC_xAOD_test-file"
# Latest JETM8 derivations with new convention for track-assisted moments
#sample = "/4/jbossios/JMS_smallR_example_MCfile"   ## USE THIS FOR JMS for small-R jets

################
# pcuba002
# JETM1
#sample = "/1/jbossios/20-7_data15_JETM1"
# Dunno
#sample = "/4/jbossios/DAOD_JETM1"
# Dunno
#sample = "/2/JETM1_TestFile"
# For Rscan
#sample = "/3/jbossios/JETM9_25ns"
# For PFlow and FatJets
#sample = "/1/New_MC15_Derivations/J8"
#sample = "/1/New_PFlow_MC_Sample"
#sample = "/2/jbossios/JetCalibTools_TestFiles/PFlow_20_7"
#sample = "/1/jbossios/Testing_JETM3_Sample"

# Rel21
#sample = "/1/jbossios/R21_MC_TestSample"
# pcuba001
#sample = "/1/jbossios/MC16_TestSample"

# Latest JETM1
#sample = "/eos/user/a/aia/Jona/TestSamples/JETM1/13Nov2017/MC16c"
sample = "/eos/user/a/aia/Jona/TestSamples/JETM1/13Nov2017/2017data"

##################
### MC or Data ###
##################
#isCollisions = False
isCollisions = True

###########
### DEV ###
###########
DEV = False
#DEV = True

#################
### CalibArea ###
#################
CalibArea = "00-04-81"

###################
### Config File ###
###################

JetCalibFiles = [
  #"Test_PFlow.config",
  #"JES_2015dataset_recommendation_Feb2016_Trigger.config",
  #"JES_Prerecommendation2015_Feb2015.config",
  #"JES_Prerecommendation2015_Feb2015_Internal.config",
  #"JES_MC15Prerecommendation_April2015.config",
  #"JES_2015dataset_recommendation_Feb2016.config",
  #"CamKt_JES_HTT.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6987_LC_Feb2016.config",
  #"JES_MC15Prerecommendation_Feb2016_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_Jan2016_trigger.config",
  #"JES_MC15Prerecommendation_Feb2016_EtaIntercalOnly_PFlow.config",
  #"JES_MC15Prerecommendation_April2015_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_April2015_withoutEtaIntercal.config",
  #"JES_MC15Prerecommendation_Rscan_Aug2015.config",
  #"JES_MC15Prerecommendation_December2015_EtaIntercalOnly.config",
  #"JES_MC15Prerecommendation_December2015_EtaIntercalOnly_PFlow.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6987_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6992_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6993_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6994_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6995_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6996_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6997_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6998_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r6999_Nov2015.config",
  #"Muscan/JES_MC15Prerecommendation_Muscan_r7000_Nov2015.config",
  #"JES_MC15Prerecommendation_PFlow_July2015.config",
  #"JES_MC15Prerecommendation_AFII_June2015.config",
  #"JES_MC15recommendation_FatJet_June2015.config",
  #"JES_MC15recommendation_FatJet_track_assisted_January2016.config",
  #"JES_MC15Prerecommendation_April2015_without_GSC.config",
  #"JES_Prerecommendation2015_AFII_Apr2015.config",
  #"JES_Full2012dataset_May2014.config",
  #"JES_Prerecommendation2015_FatJet_Apr2015.config",
  #"FatJet_JES_2012_0.config",
  #"JES_Full2012dataset_InsituDerivation.config",
  #"JES_MC15Prerecommendation_April2015_GSCEM3.config",
  #"JES_MC15Prerecommendation_April2015_GSCEM3.config",
  #"HLLHC/JES_MC15_HLLHC_r7699_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7700_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7701_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7702_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7703_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7704_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7709_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7768_May2016_v2.config",
  #"HLLHC/JES_MC15_HLLHC_r7769_May2016_v2.config",
  #"JES_MC15cRecommendation_May2016.config", # 20.7 recommendations 4EM and 4LC
  #"JES_MC15cRecommendation_PFlow_Aug2016.config",
  #"JES_2016data_Oct2016_EtaIntercalOnly.config",
  #"JES_MC15cRecommendation_May2016_JMS.config", # 20.7 recommendations 4EM and 4LC with JMS (only for 4EM)
  #"JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
  #"JES_MC15_FatJet_Nov2016_QCDCombinationCorrelatedWeightsOptional.config",
  #"JES_MC15cRecommendation_May2016_rel21.config",
  #"JES_MC15c_HI_Nov2016.config",
  #"JES_data2016_data2015_Recommendation_Dec2016.config", # Final config for 2015+2016 data
  #"JES_data2016_data2015_Recommendation_Dec2016_JMS.config", # Final config for 2015+2016 data with JMS (only for 4LC jets, no JMS for LC jets yet)
  #"JES_data2016_data2015_Recommendation_Dec2016_rel21.config", # Final config for 2015+2016 data with JMS (only for 4LC jets, no JMS for LC jets yet)
  #"JES_MC15cRecommendation_May2016.config",
  #"JES_MC15cRecommendation_PFlow_EtaIntercalOnly_March2017.config", # EtaIntercal Only config for pflow and 2016 data
  #"JES_MC16Recommendation_June2017.config", # MCJES rel21 4EM only
  #"JES_MC16Recommendation_July2017.config", # MCJES rel21 4EM only
  #"JES_MC16Recommendation_Aug2017.config", # MCJES + GSC rel21 4EM only
  #"JES_MC16Recommendation_PFlow_Aug2017.config", # MCJES + GSC rel21 4EM only
  #"JES_MC16Recommendation_Oct2017.config", # Final MCJES for MC16
  #"JES_MC16Recommendation_PFlow_Oct2017.config",
  #"JES_MC16Recommendation_Nov2017.config", # Final MCJES for MC16
  "JES_MC16Recommendation_PFlow_Nov2017.config",
]
######################
### Jet Collection ###
######################
JetCollections = [
  #"AntiKt4EMTopo",
  #"AntiKt4EMTopoTrig",
  "AntiKt4EMPFlow",
  #"AntiKt4LCTopo",
  #"AntiKt10LCTopoTrimmedPtFrac5SmallR30",
  #"AntiKt10LCTopoTrimmedPtFrac5SmallR20", # Standard FatJets
  #"AntiKt2LCTopo",
  #"AntiKt5LCTopo",
  #"AntiKt3LCTopo",
  #"AntiKt6LCTopo",
  #"CamKt020LCTopo",
  #"CamKt025LCTopo",
  #"CamKt030LCTopo",
  #"CamKt035LCTopo",
  #"CamKt040LCTopo",
  #"CamKt045LCTopo",
  #"CamKt050LCTopo",
  #"CamKt055LCTopo",
  #"CamKt060LCTopo",
  #"CamKt150LCTopo",
  #"AntiKt4HI",
  #"AntiKt2HI",
  #"AntiKt3HI",
]
######################
### Calib Sequence ###
######################
#CalibrationSequence = "JetArea_Residual"
#CalibrationSequence = "JetArea_Residual_EtaJES" #(PFlow, rel21)
CalibrationSequence = "JetArea_Residual_EtaJES_GSC" #(PFlow, rel21)
#CalibrationSequence = "JetArea_Residual_EtaJES_GSC_Insitu" #(PFlow,rel21)
#CalibrationSequence = "JetArea_Residual_Origin"
#CalibrationSequence = "JetArea_Residual_EtaJES" # rel21
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_Insitu"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_GSC"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_GSC_Insitu"
#CalibrationSequence = "JetArea_Residual_Origin_EtaJES_GSC_JMS_Insitu"
#CalibrationSequence = "JetArea_EtaJES_GSC"
#CalibrationSequence = "JetArea_EtaJES_GSC_Insitu" # Trigger
#CalibrationSequence = "EtaMassJES"
#CalibrationSequence = "EtaJES"
#CalibrationSequence = "EtaJES_JMS" # large-R jets
#CalibrationSequence = "EtaJES_Insitu" # HI jets

##################################################################################
#
##################################################################################

os.system("mkdir Outputs ErrFiles LogFiles build")
Output = "Outputs/Output_"
command = ""

for jetcoll in JetCollections:

  for config in JetCalibFiles:

    output = Output
    output += jetcoll
    output += "_"
    output += config

    command += "nohup Run "

    # For JetCalibTools
    command += "--jetCalibFile=" + config

    command += " --jetColl=" + jetcoll
  
    command += " --calibSequence=" + CalibrationSequence
    
    command += " --calibArea=" + CalibArea

    if DEV:
      command += " --DEVmode=TRUE"

    if isCollisions:
      command += " --isCollisions=TRUE"

    # Outputs
    command += " --submitDir="
    command += output

    # Sample
    command += " --sample=" + sample + " > "

    # Logfile's Path
    command += "LogFiles/log_"
    command += jetcoll
    command += "_"
    command += config
    command += " 2> "

    # Errors File's Path
    command += "ErrFiles/errors_"
    command += jetcoll
    command += "_"
    command += config
    command += " && "

command = command[:-2]
print command
os.system(command)

